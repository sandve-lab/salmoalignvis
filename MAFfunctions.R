# reads a maf file and returns as a table
#
# "idx" is the unique index for each multiple alignment
# "seqLen" is the length of the input seqence block, which is length of the gene + 100k on each side
# "pos" is the start of the alignment the within the input sequence block
# "len" is the length of the sequence in the alignment (with no insertions)
# if "strand" is - then the position refers to the position on the reverse sequence
readMAF <- function(mafFile){
  x <- readLines(mafFile)
  
  aLines <- grepl("^a",x)
  sLines <- grepl("^s",x)
  
  tibble(idx=cumsum(aLines),txt=x) %>% 
    filter(sLines) %>%
    # separate(txt, into=c("s","geneID","pos","len","strand","seqLen","seq"),sep = "[ \t]+") %>% 
    separate(txt, into=c("s","src","start","size","strand","srcSize","text"),sep = "[ \t]+") %>% 
    select(-s) %>% 
    # mutate(geneID = sub("\\..*$","",geneID)) %>% 
    # mutate_at(vars(pos,len,seqLen),as.numeric) 
    mutate_at(vars(start,size,srcSize),as.integer) 
}


# readMAF with EPO metadata (id and tree)
readMAF_EPO <- function(mafFile){
  x <- readLines(mafFile)
  
  # In the MAF format each multiple alignment block starts with an "^a" line 
  # followed by "^s" lines for each sequence in the alignment. 
  # (there are also i,e and q, but I havent seen those in the EPO alignments)
  
  # The EPO alignments also include "# tree:" and "# id:" preceding each block
  # and a "# gerp scores:" line after each block
  
  # use the tree lines to identify the start of each block
  treeLines <- grepl("^# tree:", x)
  
  tibble(idx=cumsum(treeLines),txt=x) %>% 
    filter(idx>0) %>% 
    group_by(idx) %>% 
    mutate( id = sub("# id: ","",txt[2])) %>% # the id line should be directly after the tree line
    mutate( tree = sub("# tree: ","",txt[1])) %>% # the tree line is the first line
    ungroup() %>% 
    filter(grepl("^s",txt)) %>%
    # use the column names as defined in https://genome.ucsc.edu/FAQ/FAQformat.html#format5
    separate(txt, into=c("s","src","start","size","strand","srcSize","text"),sep = "[ \t]+") %>% 
    select(-s) %>% 
    mutate_at(vars(start,size,srcSize),as.integer) 
}



# Change coordinates relative to TSS
# relTSSposMAF <- function(MAtbl, posTbl){
#   MAtbl %>% 
#     left_join(select(posTbl, geneID, startGene, endGene, startBlock, endBlock, orientation), by=c("src"="geneID")) %>% 
#     mutate( start = start - ifelse(orientation=="-",endBlock-endGene,startGene-startBlock))
# }

# Change coordinates of OG alignments (MAtbl) relative to chromosome
# OG alignments are done on a region (block) 100kb around each gene so
# the coordinates in the alignmnents are relative to these blocks.
# Positions of the genes and blocks are given in posTbl
relCHROMposMAF <- function(MAtbl, posTbl){
  MAtbl %>% 
    mutate(geneID = sub("\\..*$","",src)) %>% 
    left_join(select(posTbl, geneID, chromosome, startGene, endGene, startBlock, endBlock, orientation, lengthChromosome), by="geneID") %>% 
    # left_join(posTbl, by="geneID") %>% 
    # if strand = "-" we count start from the end of the chromosome
    # startBlock is one-based so subtract 1
    mutate( start = ifelse(strand=="-",start+lengthChromosome-endBlock,start+startBlock-1)) %>% 
    mutate( src = chromosome) %>% 
    mutate( srcSize = lengthChromosome) %>% 
    select( idx, src, start, size, strand, srcSize, text)
}


# # returns a vector for each gene containing the aligned coordinates within the other gene
# # pos.x, pos.y, base (single character if match or e.g. A/T if mismatch)
# getPairwiseAlignedBases <- function(MAtbl, gene_x, gene_y){
#   tmp <-
#     MAtbl %>% 
#     select(idx,geneID,pos,seq,strand) %>% 
#     # select only the alignments between gene_x and gene_y
#     filter( geneID %in% c(gene_x,gene_y)) %>% 
#     filter(strand=="+") %>% 
#     # only alignments with both genes
#     group_by(idx) %>% 
#     filter( all(c(gene_x,gene_y) %in% geneID) ) %>% 
#     ungroup() %>% 
#     rename( start=pos )
#   
#   # table(tmp$strand)
#   # if(any(tmp$strand!= "+")) warning("reversed alignment not implemented yet!")
#   
#   
#   # There can be one-to-many alignments. e.g. id=1 includes c(gene_x, gene_y, gene_y)
#   # To deal with this, split the table into into two tables (gene_x and gene_y) and
#   # join so each row becomes one unique pairwise alignment with distinct columns for x and y data
#   pairAlnTbl <- 
#     full_join(filter(tmp,geneID==gene_x), 
#               filter(tmp,geneID==gene_y),by="idx")
#   
#   # get positions of all aligned bases
#   pairAlnTbl %>% 
#     # for each alignment get a table with one row for each aligned basepair and columns
#     # pos.x, pos.y, mismatch
#     mutate( nestedTbl = map2(
#       .x=seq.x, .y=seq.y, 
#       ~ as_tibble(strsplit(c(x=.x,y=.y),split = "")) %>% 
#         mutate(pos.x=cumsum(x!="-"),pos.y=cumsum(y!="-")) %>% 
#         filter(x!="-", y!="-") %>% 
#         mutate(mismatch=x!=y) %>% 
#         mutate(base=ifelse(mismatch,paste(x,y,sep = "/"),x)) %>% 
#         select(-x,-y) 
#     )) %>% 
#     select( -seq.x, -seq.y) %>% # dont need these anymore
#     filter( map_int(nestedTbl, nrow) != 0) %>% # remove alignments with 0 matches
#     unnest(nestedTbl) %>% 
#     # add start pos
#     mutate( pos.x = pos.x + start.x - 1,
#             pos.y = pos.y + start.y - 1) %>% 
#     select( idx, pos.x, pos.y,base,mismatch)
# }

# calulate binned alignment stats
#
#
binAlign <- function(alnBases, binsize=1000){
  alnBases %>% 
    # mutate(delta=abs(pos.x-pos.y)) %>% 
    gather(key = "gene",value = "pos",pos.x,pos.y) %>% 
    mutate( bin= pos - (pos %% binsize)) %>%
    group_by(bin) %>%
    summarise( nAligned=n(), nMismatch=sum(mismatch))  
  # summarise( nAligned=n(), nMismatch=sum(mismatch), delta=mean(delta))  
}


# # load alignment and calulate binned alignment stats
# getBinAlign <- function(N3,gene_x,gene_y, minPos=-5000, maxPos=1000,binSize=100){
#   mafFile <- paste0("/mnt/SCRATCH/thut/alignments_salmonids/alignments_N3/",N3,".maf")
#   tryCatch({
#     binTbl <- 
#       readMAF(mafFile) %>% 
#       relTSSposMAF(posTblAll) %>% 
#       getPairwiseAlignedBases(gene_x,gene_y) %>% 
#       # filter( (pos.x >= minPos & pos.x < maxPos) | (pos.y >=minPos & pos.y < maxPos)) %>% 
#       select(-idx) %>% distinct() %>% # remove duplicate alignments
#       binAlign(binsize = binSize) %>% 
#       filter( bin >= minPos & bin < maxPos ) 
#   }, error = function(e){
#     cat("Error in: N3='",N3,"'; gene_x='",gene_x,"'; gene_y='",gene_y,"'\n",sep = "")
#     # tibble(bin=double(), nAligned=integer(), nMismatch=integer())
#     tibble(bin=NA_real_, nAligned=NA_integer_, nMismatch=NA_integer_) # add a NA entry to distinguish between error and no matches
#   })
# }



# Get pairwise aligned bases between two given regions.
#
# This might not be the most efficient way but is a simple interface
#
# start/end is one-based. e.g. start=1 and end=5 returns the first 5 bases
# returned pos.x/pos.y is also one-based
getAlnBases <- function(MAtbl,src_x, start_x, end_x, src_y, start_y, end_y){
  # cat( "x:",src_x, start_x, end_x, "\ny:",src_y, start_y, end_y,"\n")
  # ranges as a table
  queryRanges <-
    tibble( xy=c("x","y"),srcQuery=c(src_x,src_y),startQuery=c(start_x,start_y), endQuery=c(end_x,end_y))
  
  
  MAtbl_overlap <- 
    MAtbl %>%
    # convert alignment positions to one-based relative to positive strand and start<end
    mutate( startAlign = ifelse(strand=="-",srcSize-start-size+1,start+1)) %>% 
    mutate( endAlign = startAlign+size-1) %>% 
    # get alignments that overlap with the query ranges
    genome_inner_join( queryRanges, by=c("src"="srcQuery","startAlign"="startQuery","endAlign"="endQuery")) %>% 
    # only reciprocal alignments (i.e. alignments must overlap both query ranges)
    group_by(idx) %>% filter( all(c("x","y") %in% xy)) %>% ungroup()
  
  # TODO: Handle zero matches
  if( nrow(MAtbl_overlap) == 0 ){
    warning("no overlapping alignments!")
    return(tibble(idx=character(0), pos.x=numeric(0), pos.y=numeric(0), base=character(0),
                  mismatch=logical(0), strand.x=character(0), strand.y=character(0)))
  }
  
  # Handle multiple alignments
  if( nrow(MAtbl_overlap) != nrow(select(MAtbl_overlap,xy,idx) %>% distinct())){
    # just select the first alignment
    MAtbl_overlap <-
      MAtbl_overlap %>% 
      group_by(xy,idx) %>% slice(1) %>% ungroup()
  }
  
  # pivot to wide table per pairwise alignment
  pairAlnTbl <-  
    MAtbl_overlap %>% 
    select( xy, start, size, strand, srcSize, text, idx) %>% 
    mutate( text = toupper(text)) %>% 
    pivot_wider( values_from = c(start, size, strand, srcSize, text),id_cols = idx,names_from = xy,names_sep = ".")  
  
  # do the per nucleotide thing
  pairAlnTbl %>% 
    # for each alignment get a table with one row for each aligned basepair and columns
    # pos.x, pos.y, base, mismatch
    mutate( nestedTbl = map2(
      .x=text.x, .y=text.y, 
      ~ as_tibble(strsplit(c(x=.x,y=.y),split = "")) %>% 
        mutate(pos.x=cumsum(x!="-"),pos.y=cumsum(y!="-")) %>% 
        filter(x!="-", y!="-") %>% 
        mutate(mismatch=x!=y) %>% 
        mutate(base=ifelse(mismatch,paste(x,y,sep = "/"),x)) %>% 
        select(-x,-y) 
    )) %>% 
    select( -text.x, -text.y) %>% 
    filter( map_int(nestedTbl, nrow) != 0) %>% # remove alignments with 0 matches
    unnest(nestedTbl) -> tmp

  # Stop if no matches in alignments
  if( nrow(tmp) == 0 ){
    warning("no overlapping bases in alignment!")
    return(tibble(idx=character(0), pos.x=numeric(0), pos.y=numeric(0), base=character(0),
                  mismatch=logical(0), strand.x=character(0), strand.y=character(0)))
  }
  
  alnBases <-
    tmp %>% 
    # add start pos
    mutate( pos.x = pos.x + start.x,
            pos.y = pos.y + start.y) %>%
    # correct position by strand
    mutate( pos.x = ifelse( strand.x=="+", pos.x, srcSize.x-pos.x+1 ),
            pos.y = ifelse( strand.y=="+", pos.y, srcSize.y-pos.y+1 )) %>% 
    select( idx, pos.x, pos.y,base,mismatch, strand.x,strand.y)
  
  # crop nucleotides outside query regions
  alnBases <- 
    alnBases %>% 
    filter( pos.x %>% between(start_x,end_x)) %>% 
    filter( pos.y %>% between(start_y,end_y))
  
  # TODO: Handle zero matches
  if( nrow(alnBases) == 0 ){
    warning("no bases overlapping query in alignments!")
  }
  
  return(alnBases)
}

# count number of aligned bases
countAlnBases <- function(MAtbl,src_x, start_x, end_x, src_y, start_y, end_y){
  alnBases <- getAlnBases(MAtbl,src_x, start_x, end_x, src_y, start_y, end_y)
  
  tibble( nAligned=nrow(alnBases), nMismatch=sum(alnBases$mismatch), 
          propAlign_x=nrow(alnBases)/(1+end_x-start_x),
          propAlign_y=nrow(alnBases)/(1+end_y-start_y))
}



